import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


public class LogicController {

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy - HH:mm:ss");
        System.out.println("How many repeated?");
        double input_user = input.nextDouble();
        input.nextLine();
        ArrayList<LocalDateTime> list = new ArrayList<>();
        for(int i = 0; i < input_user; i++){
            System.out.print("Put your datetime (MMM dd, yyyy - HH:mm:ss) : ");
            String input_time = input.nextLine();
            LocalDateTime parsed = LocalDateTime.parse(input_time, formatter);
            list.add(parsed);
        }
        MyClass my = new MyClass();
        my.setList(list);
        Thread thread = new Thread(my);
        thread.start();
        input.close();
    }

}

class MyClass implements Runnable{

    private LocalDateTime date;
    private ArrayList<LocalDateTime> list;

    public void setList(ArrayList<LocalDateTime> newList) {
        list = newList;
    }


    public void run(){
        LocalDateTime localDateTime;
        while(true) {
            localDateTime = LocalDateTime.now();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isEqual(localDateTime))
                {
                    System.out.println("Alarm Bunyi");
                    list.remove(list.get(i));
                }
                else if(list.get(i).isBefore(localDateTime))
                {
                    System.out.println("Sudah lewat dengan waktu " + list.get(i).toString());
                    list.remove((list.get(i)));
                }
            }

            if (list.isEmpty()) break;
        }
    }

    public MyClass(LocalDateTime date){
        this.date = date;
    }

    public MyClass() {}
}